#!/bin/bash

id=`docker ps -q -f name=revee_cassandra `

[ -n "$id" ]  && docker stop $id 
docker rm revee_cassandra 2> /dev/null

docker run -p 7000:7000 -p 7001:7001 -p 9042:9042 -p 9160:9160  -d --name  revee_cassandra  revee_cassandra

