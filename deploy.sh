#!/bin/bash
source config.sh
./build.sh

docker login -u ${artifactoryUser} -p ${artifactoryPassword}  accessmc-docker-local.jfrog.io
docker tag $FORCE revee_cassandra accessmc-docker-local.jfrog.io/revee_cassandra:${DATASTORE_VERSION}
docker push accessmc-docker-local.jfrog.io/revee_cassandra:${DATASTORE_VERSION}

if [ -n "$GIT_COMMIT" ] ; then
    docker tag $FORCE revee_cassandra accessmc-docker-local.jfrog.io/revee_cassandra:$GIT_COMMIT
    docker push accessmc-docker-local.jfrog.io/revee_cassandra:$GIT_COMMIT
fi
