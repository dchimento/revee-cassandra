#!/bin/bash
# get login info
source config.sh

curl -s  -u${artifactoryUser}:${artifactoryPassword} https://accessmc.artifactoryonline.com/accessmc/libs-releases-local/com/accessmedia/amc-datastore/${DATASTORE_VERSION}/amc-datastore-${DATASTORE_VERSION}.jar > /tmp/datastore.jar
[ -d schema ] || mkdir schema 
(
    cd schema ;
    rm -f *.cql 
    jar xf /tmp/datastore.jar pageviewslog.cql page_unsupported_tables.cql page.cql recommendation.cql tvr.cql
    sed -i.bak 's/view counter,/impression counter,/g' tvr.cql
    sed -i.bak '1,3d' page_unsupported_tables.cql
    rm -f *.bak
)

docker build --no-cache -t revee_cassandra .
