#!/bin/bash

set -x
echo "Waiting for cassandra to come up"
sleep 15;

for i in /data/*.cql; do
cqlsh -f $i
done
