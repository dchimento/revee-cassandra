from cassandra:2

RUN mkdir /data
ADD schema/*.cql /data/
ADD zzz_simple_replication.cql /data/
ENTRYPOINT ["/data/entrypoint.sh"]
COPY entrypoint.sh /data/entrypoint.sh
RUN chmod 755 /data/entrypoint.sh

ADD load-schema.sh /data/load-schema.sh
RUN chmod 755 /data/load-schema.sh

# 7000: intra-node communication
# 7001: TLS intra-node communication
# 7199: JMX
# 9042: CQL
# 9160: thrift servic

EXPOSE 7000 7001 7199 9042 9160
CMD ["cassandra", "-f"]

